package ru.tsc.gavran.tm.api.controller;

import ru.tsc.gavran.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void showFindProject(Project project);

    void clearProjects();

    void createProject();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByName();

    void changeStatusByIndex();

}