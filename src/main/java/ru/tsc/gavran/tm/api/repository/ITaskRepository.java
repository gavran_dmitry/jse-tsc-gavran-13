package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    boolean existsById(String id);

    List<Task> findAllTaskByProjectId(final String id);

    Task bindTaskToProjectById(final String projectId, final String taskId);

    Task unbindTaskById(final String id);

    void unbindAllTaskByProjectId(final String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    Task startById(String id);

    Task startByName(String name);

    Task startByIndex(int index);

    Task finishById(String id);

    Task finishByName(String name);

    Task finishByIndex(int index);

    Task changeStatusById(String id, Status status);

    Task changeStatusByName(String name, Status status);

    Task changeStatusByIndex(int index, Status status);

    void clear();

}