package ru.tsc.gavran.tm.controller;

import ru.tsc.gavran.tm.api.controller.IProjectController;
import ru.tsc.gavran.tm.api.service.IProjectService;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showFindProject(Project project) {
        System.out.println("Id:" + project.getId());
        System.out.println("Name:" + project.getName());
        System.out.println("Description:" + project.getDescription());
        System.out.println("Status:" + project.getStatus());
    }

    @Override
    public void updateByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) System.out.println("INCORRECT VALUES!");
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) System.out.println("INCORRECT VALUES!");
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.startById(id);
        System.out.println("[OK]");
    }

    @Override
    public void startByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.startByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void startByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.startByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void finishById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.finishById(id);
        System.out.println("[OK]");
    }

    @Override
    public void finishByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.finishByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void finishByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.finishByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void changeStatusById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.changeStatusById(id, status);
        System.out.println("[OK]");
    }

    @Override
    public void changeStatusByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.changeStatusByName(name, status);
        System.out.println("[OK]");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        projectService.changeStatusByIndex(index, status);
        System.out.println("[OK]");
    }

}