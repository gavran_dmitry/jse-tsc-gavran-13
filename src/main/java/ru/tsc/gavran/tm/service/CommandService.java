package ru.tsc.gavran.tm.service;

import ru.tsc.gavran.tm.api.repository.ICommandRepository;
import ru.tsc.gavran.tm.api.service.ICommandService;
import ru.tsc.gavran.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}